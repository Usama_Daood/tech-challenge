package com.lalamove.www.assignmentlalamove.Adapter;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.lalamove.www.assignmentlalamove.R;
import com.lalamove.www.assignmentlalamove.volley.CustomVolleyRequest;

import java.util.ArrayList;

public class adapter extends BaseAdapter {

    //Imageloader to load images
    private ImageLoader imageLoader;

    //Context
    private Context context;

    private LayoutInflater mInflater;

    //Array List that would contain the urls and the titles for the images
    private ArrayList<String> images;
    private ArrayList<String> names;
    private ArrayList<Double> latitue;
    private ArrayList<Double> longitute;
    public adapter(Context context,ArrayList<String> images, ArrayList<String> names,  ArrayList<Double> latitue, ArrayList<Double>  longitute){
        //Getting all the values
        this.context = context;
        this.images = images;
        this.names = names;
        this.latitue = latitue;
        this.longitute = longitute;
        mInflater = LayoutInflater.from(context);

    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public Object getItem(int position) {
        return images.get(position);
    }




    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //Creating a linear layout
//        LinearLayout linearLayout = new LinearLayout(context);
//        linearLayout.setOrientation(LinearLayout.HORIZONTAL);

        convertView = mInflater.inflate(R.layout.grid_item, null);

        NetworkImageView networkImageView = (NetworkImageView) convertView.findViewById(R.id.pic);
        TextView desc=(TextView) convertView.findViewById(R.id.desc);

        //NetworkImageView
//        NetworkImageView networkImageView = new NetworkImageView(context);

        try{
            //Initializing ImageLoader
            imageLoader = CustomVolleyRequest.getInstance(context).getImageLoader();
            imageLoader.get(images.get(position), ImageLoader.getImageListener(networkImageView, R.drawable.user, R.drawable.user));

            //Setting the image url to load
            networkImageView.setImageUrl(images.get(position),imageLoader);

            desc.setText(""+names.get(position)+"\n");
            //Creating a textview to show the title
//            TextView textView = new TextView(context);
//            textView.setGravity(Gravity.CENTER);
//            textView.setText("");

            //Scaling the imageview
            networkImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            //networkImageView.setLayoutParams(new GridView.LayoutParams(400,400));

            //Adding views to the layout

//            linearLayout.addView(networkImageView);
//            linearLayout.addView(textView);
            //Returnint the layout


        }catch(Exception r){
            Toast.makeText(context, ""+r.toString(), Toast.LENGTH_SHORT).show();
        }


        return convertView;
    }





}
