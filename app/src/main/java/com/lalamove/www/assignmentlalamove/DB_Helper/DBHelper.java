package com.lalamove.www.assignmentlalamove.DB_Helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

public class DBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "deliveries.db";
    public static final String deliveries_TABLE_NAME = "user_locations";
    public static final String deliveries_COLUMN_ID = "id";
    public static final String deliveries_COLUMN_Description = "description";
    public static final String deliveries_COLUMN_LAT = "lati";
    public static final String deliveries_COLUMN_LNG = "lng";
    public static final String deliveries_COLUMN_IMG = "img";


    // Tag table create statement
    private static final String CREATE_TABLE_TAG = "CREATE TABLE " + deliveries_TABLE_NAME
            + "(" + deliveries_COLUMN_ID + " INTEGER PRIMARY KEY," + deliveries_COLUMN_Description + " TEXT," +"" +
            "" +deliveries_COLUMN_LAT +"TEXT, "+
            " "+ deliveries_COLUMN_LNG+ " TEXT," +
            "" + deliveries_COLUMN_IMG +"TEXT "+
            " )";


    public DBHelper(Context context) {
        super(context, DATABASE_NAME , null, 4);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub

//        db.execSQL(CREATE_TABLE_TAG);

        db.execSQL(
                "create table user_locations " +
                        "(id integer primary key, description text,lati text,lng text, img text)"
        );



    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS "+deliveries_TABLE_NAME);
        onCreate(db);
    }



    ///////////// Add all values of location_later_display

    public long off_line_save ( String desc, Double lat,Double lng,String img) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(deliveries_COLUMN_Description, desc);
        contentValues.put(deliveries_COLUMN_LAT, lat);
        contentValues.put(deliveries_COLUMN_LNG, lng);
        contentValues.put(deliveries_COLUMN_IMG, img);
        long ij=db.insert(deliveries_TABLE_NAME, null, contentValues);
        return ij;
    }


    public int numberOfRows(){
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, deliveries_TABLE_NAME);
        return numRows;
    }

    public void delete_all_desc () {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from "+deliveries_TABLE_NAME);
    }











}
