package com.lalamove.www.assignmentlalamove;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.StrictMode;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.lalamove.www.assignmentlalamove.Adapter.adapter;
import com.lalamove.www.assignmentlalamove.DB_Helper.DBHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;

import static com.lalamove.www.assignmentlalamove.DB_Helper.DBHelper.deliveries_COLUMN_Description;
import static com.lalamove.www.assignmentlalamove.DB_Helper.DBHelper.deliveries_COLUMN_IMG;
import static com.lalamove.www.assignmentlalamove.DB_Helper.DBHelper.deliveries_COLUMN_LAT;
import static com.lalamove.www.assignmentlalamove.DB_Helper.DBHelper.deliveries_COLUMN_LNG;
import static com.lalamove.www.assignmentlalamove.DB_Helper.DBHelper.deliveries_TABLE_NAME;

public class home extends AppCompatActivity {
    Spinner cato;
    String name,cate_id;
    Button btn;
    JSONArray jsonArray,jsonArray2;
    ProgressDialog loading;
    ArrayList<String> cate_list = new ArrayList<String>();
    ArrayList<String> cate_name = new ArrayList<String>();
    ArrayList<String> new_cate_ids = new ArrayList<String>();
    String mydate;
    InputStream is = null;
    String line = null;
    String result = null;
    static Bitmap bmOverlay;
//    public static final String DATA_URL = "http://baithek.com/hadia/api/all_design.php";

    public static final String DATA_URL = "http://5b9b8e548d1635001482ccd7.mockapi.io/assign/deliveries";

//      Database  Variables

    DBHelper local_db;

    //////// End data base Variabels

    SQLiteDatabase db;
    public static final String TAG_IMAGE_URL = "imageUrl";
    public static final String TAG_NAME = "description";
    public static final String TAG_LATITUTE = "latitute";
    public static final String TAG_LONG = "longitute";
    //GridView Object
    private GridView gridView;

    //ArrayList for Storing image urls and titles
    private ArrayList<String> images;
    private ArrayList<String> names;
    private ArrayList<String> address_array;
    private ArrayList<Double> latitute;
    private ArrayList<Double> longitute;
    String picturePath;
    HorizontalScrollView scrollView;
    LinearLayout ll;

    private void checkRunTimePermission() {
        String[] permissionArrays = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.INTERNET};

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissionArrays, 11111);
        } else {
            // if already permition granted
            // PUT YOUR ACTION (Like Open cemara etc..)
        }
    }
    String img,desc;
    Double lat,lng;
    boolean isConnected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        local_db = new DBHelper(this);

        setTitle("Things to Deliver");

        checkRunTimePermission();

        images = new ArrayList<String>();
        names = new ArrayList<String>();
        latitute = new ArrayList<Double>();
        longitute = new ArrayList<Double>();
        address_array=new ArrayList<String>();
        gridView = (GridView) findViewById(R.id.gridView);

        /// Check Interne
        ConnectivityManager cm = (ConnectivityManager) home.this.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();


        if(isConnected==true){
            ///// Internet Connected (Online)

            clear_arrays();

            local_db.delete_all_desc();


            getData();

        }else{
            ////// Internet is not connected (Offline)
//////Get Data from Local Storage

            Toast.makeText(this, "You are offline, Data display from Local Storage.", Toast.LENGTH_SHORT).show();


            db = local_db.getReadableDatabase();

            Cursor res =  db.rawQuery( "select * from "+deliveries_TABLE_NAME, null );
            res.moveToFirst();


            while(res.isAfterLast() == false){

                //String local_lat= res.getString(res.getColumnIndex(CONTACTS_COLUMN_USER_LAT));

                img= res.getString(res.getColumnIndex(deliveries_COLUMN_IMG));
                desc= res.getString(res.getColumnIndex(deliveries_COLUMN_Description));
                lat= Double.valueOf(res.getString(res.getColumnIndex(deliveries_COLUMN_LAT)));
                lng= Double.valueOf(res.getString(res.getColumnIndex(deliveries_COLUMN_LNG)));


                images.add(img);
                names.add(desc);
                latitute.add(lat);
                longitute.add(lng);


              //  Toast.makeText(home.this, "deliveries_COLUMN_Description  "+desc, Toast.LENGTH_SHORT).show();

                res.moveToNext();

            }

            int ok=local_db.numberOfRows();

            Toast.makeText(this, "# of Rows "+ok, Toast.LENGTH_SHORT).show();




            try{

                adapter gridViewAdapter = new adapter(this,images,names,latitute,longitute);
                //Adding adapter to gridview
                gridView.setAdapter(gridViewAdapter);
//        gridView2.setAdapter(gridViewAdapter);


            }catch (Exception t){
                Toast.makeText(this, "error "+t.toString(), Toast.LENGTH_SHORT).show();
            }



            /////// End Data From Local DB


        }

        ///// End check internet



        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);

//        images = new ArrayList<>();
//        names = new ArrayList<>();
//        latitute = new ArrayList<>();
//        new_cate_ids = new ArrayList<>();
//        longitute = new ArrayList<>();
////        getData();



    }

    private void getData(){
        //Showing a progress dialog while our app fetches the data from url
        final ProgressDialog loading = ProgressDialog.show(this, "Please wait...","Fetching data...",false,false);

        //Creating a json array request to get the json from our api
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(DATA_URL,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        //Dismissing the progressdialog on response
                        loading.dismiss();
                        // Toast.makeText(MainActivity.this, ""+response, Toast.LENGTH_SHORT).show();
                        //Displaying our grid
                        showGrid(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }                }
        );

        //Creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        //Adding our request to the queue
        requestQueue.add(jsonArrayRequest);
    }



    private void showGrid(final JSONArray jsonArray){
        //Looping through all the elements of json array
        for(int i = 0; i<jsonArray.length(); i++){
            //Creating a json object of the current index
            JSONObject obj = null;
            try {
                //getting json object from current index
                obj = jsonArray.getJSONObject(i);

                /// Variables
                String img,desc,address;
                Double lat,lng;
                img=obj.getString(TAG_IMAGE_URL);
                desc=obj.getString(TAG_NAME);

                JSONObject phone = obj.getJSONObject("location");
                lat = phone.getDouble("lat");
                lng = phone.getDouble("lng");
                address=phone.getString("address");

                //getting image url and title from json object
                images.add(img);
                names.add(desc);
                latitute.add(lat);
                longitute.add(lng);
                address_array.add(address);
                /////// Add DB in Local Database.

            local_db.off_line_save(desc,lat,lng,img);
//                Toast.makeText(this, ""+che, Toast.LENGTH_SHORT).show();

               //////////   End Add Local DB


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        //Creating GridViewAdapter Object
        adapter gridViewAdapter = new adapter(this,images,names,latitute,longitute);

        //Adding adapter to gridview
        gridView.setAdapter(gridViewAdapter);
//        gridView2.setAdapter(gridViewAdapter);


        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    final int position, long id) {



                try {

                    final ProgressDialog loading1 = ProgressDialog.show(home.this, "Please wait...","Open Detail...",false,false);

                    JSONObject obj = jsonArray.getJSONObject(position);
                    obj.getString(TAG_IMAGE_URL);
//                    Toast.makeText(home_new.this, "" + position + " " + obj.getString(TAG_IMAGE_URL), Toast.LENGTH_SHORT).show();


                    Intent intent = new Intent(getBaseContext(), MapsActivity.class);
                    intent.putExtra("link", obj.getString(TAG_IMAGE_URL));
                    intent.putExtra("description", obj.getString(TAG_NAME));
                    intent.putExtra("latitute", obj.getDouble(TAG_LATITUTE));
                    intent.putExtra("longitute", obj.getDouble(TAG_LONG));
                    startActivity(intent);

                    loading1.dismiss();

                } catch (Exception p) {
                    Toast.makeText(home.this, "Runtime Error", Toast.LENGTH_SHORT).show();

                }
            }

//                    try{
//
//                        URL newurl = new URL(obj.getString(TAG_IMAGE_URL).toString());
//                        mIcon_val = BitmapFactory.decodeStream(newurl.openConnection().getInputStream());
//                        imageView.setImageBitmap(mIcon_val);
//                        imageView.setMaxHeight(1000);
//                        imageView.setMaxWidth(1000);
//                        imageView.getLayoutParams().height = 20;
//                        imageView.getLayoutParams().width = 20;
//
//                        View.OnTouchListener t = new View.OnTouchListener()
//                        {
//                            public boolean onTouch(View paramView, MotionEvent event)
//                            {
//                                ImageView view = (ImageView)paramView;
//                                switch (event.getAction() & MotionEvent.ACTION_MASK)
//                                {
//                                    case MotionEvent.ACTION_DOWN:
//
//                                        savedMatrix.set(matrix);
//                                        start.set(event.getX(), event.getY());
//                                        Log.d(TAG, "mode=DRAG" );
//                                        mode = DRAG;
//                                        break;
//                                    case MotionEvent.ACTION_POINTER_DOWN:
//
//                                        oldDist = spacing(event);
//                                        Log.d(TAG, "oldDist=" + oldDist);
//                                        if (oldDist > 10f) {
//
//                                            savedMatrix.set(matrix);
//                                            midPoint(mid, event);
//                                            mode = ZOOM;
//                                            Log.d(TAG, "mode=ZOOM" );
//                                        }
//                                        break;
//
//                                    case MotionEvent.ACTION_MOVE:
//
//                                        if (mode == DRAG) {
//
//                                            matrix.set(savedMatrix);
//                                            matrix.postTranslate(event.getX() - start.x, event.getY() - start.y);
//                                        }
//                                        else if (mode == ZOOM) {
//
//                                            float newDist = spacing(event);
//                                            Log.d(TAG, "newDist=" + newDist);
//                                            if (newDist > 10f) {
//
//                                                matrix.set(savedMatrix);
//                                                float scale = newDist / oldDist;
//                                                matrix.postScale(scale, scale, mid.x, mid.y);
//                                            }
//                                        }
//                                        break;
//
//                                    case MotionEvent.ACTION_UP:
//                                    case MotionEvent.ACTION_POINTER_UP:
//
//                                        mode = NONE;
//                                        Log.d(TAG, "mode=NONE" );
//                                        break;
//                                }
//                                view.setImageMatrix(matrix);
//                                return true;
//
//                            }
//
//                            private void midPoint(PointF point, MotionEvent event) {
//
//                                float x = event.getX(0) + event.getX(1);
//                                float y = event.getY(0) + event.getY(1);
//                                point.set(x / 2, y / 2);
//                            }
//
//                            private float spacing(MotionEvent event) {
//                                float x = event.getX(0) - event.getX(1);
//                                float y = event.getY(0) - event.getY(1);
//                                return (float) Math.sqrt(x * x + y * y);
//                            }
//                        };
//
//
//                        imageView.setScaleType(ImageView.ScaleType.MATRIX);
//
//                        imageView.setOnTouchListener(t);
//


//                        try{
//                            overlay(BitmapFactory.decodeFile(picturePath),mIcon_val);
//                        }catch (Exception r){
//                            Toast.makeText(MainActivity.this, "Merging Error "+r.toString(), Toast.LENGTH_SHORT).show();
//                        }


//
//
//
//                    }catch(Exception t ){
//                        Toast.makeText(MainActivity.this, ""+t.toString(), Toast.LENGTH_SHORT).show();
//                    }
//
//
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
////                } catch (MalformedURLException e) {
////                    e.printStackTrace();
////                } catch (IOException e) {
////                    e.printStackTrace();
//                }


            //}

        });



    }



    ///// Menu Option

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        MenuItem item = menu.findItem(R.id.refresh);

        if (isConnected == true) {
            /// Enabled
            item.setEnabled(true);
            item.getIcon().setAlpha(255);
        } else {
            // disabled
            item.setEnabled(false);
            item.getIcon().setAlpha(130);
        }
        return true;
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.top_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.refresh) {
            clear_arrays();
            local_db.delete_all_desc();
            refresh();
        }

        return super.onOptionsItemSelected(item);
    }
    //// End menu Option


    ///// Refresh Now Code is here.

    public void refresh(){
//// Clear Arrays
        clear_arrays();

///// End clear Arrays

///=======  Get Latest Record from API's

            getData();

//// -----  End Get Latest Record From API's

    }

    /////////// ENd Refresh Code





    public void clear_arrays(){
        names.clear();
        latitute.clear();
        longitute.clear();
        images.clear();

    }



}
